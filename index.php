<?php
	require('db.php');
	$records_per_page	=	12;
	$query				=	"SELECT COUNT(name) AS count FROM messages";
	$result				=	mysqli_query($db,$query) or die(mysql_error());
	$row				= 	mysqli_fetch_array($result,MYSQLI_ASSOC);
	$count				=	$row['count'];
	$total_pages		=	ceil($count / $records_per_page);
?>
<!DOCTYPE HTML>
<head>
<link href='load-more.css' rel='stylesheet' type='text/css'/>
<script src="jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="load-more.js" type="text/javascript"></script>
<script type="text/javascript">
	var total_pages	=	<?php echo $total_pages; ?>;
</script>
</head>
<body>
<div id="container">
	<div id="contact-box">
		<ul id="messages-list">
		</ul>
	</div>
</div>
</body>
</html>